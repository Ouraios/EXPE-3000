# EXPE-3000
Compteur d'expédition sur ogame

Bugs connus:
------------
* Mauvaise reconnaissance des messages si ceux-cis s'affichent sur plusieurs pages ET que le temps passé sur une page est < 'config_user.delaiActualisation'
* Si les variables 'conservationMessages' mal configuré, des messages faux peuvent apparaître
* Parfois, le rapport d'expédition ne s'ajoute pas du premier coup (signalé v1.x)
* Un nouveau RExp est mal reconnu si une fenêtre 'RC détaillé' est ouverte à côté
* Les RExp ne s'ajoutent pas si une fenêtre RC détaillée est restée ouverte
* Nombre expé par jour abbérant pour un petit nombre d'expés enregistrées

ChangeLog:
----------
###v3.16 (05/08/2015) : 

* Mise à jour du script const passé en var pour compatibilité avec FF

###v3.15 (27/07/2015) : 

* Mise à jour du script avec nouveaux liens de téléchargement du script
* BBCode de nouveau fonctionnel

###v3.14 : 

* Vérification mise à jour automatique
* Ajout phrase clé

###v3.13:
* Ajout phrase clé

###v3.12:
* Ajout phrase clé

###v3.11:
* Ajout phrase clé

###v3.10:
* Amélioration de l'ordonnement liste_positions
* Changement de pseudo possible
* Phrase clé ajoutée

###v3.9.1:
* Corrections ordonnement liste_position
* Espace avant unité (k/m/g)
* Corrections de texte

###v3.9:
* Modification texte
* Correction: des RExp ne s'affichaient pas comme 'à sauvegarder"
* Compatibilité option 'RC détaillé dans fenêtre'
* Modification aspect liste_position
* La liste des position apparaît dans l'ordre (MAJ/ordonnement automatique/nettoyage de la liste)
* Lien image bouton BBCode incorrect
* L'affichage peut être écrit sous forme raccourcie pour les grands nombres
* Bug sur page userscripts
* Nouvelle stat: combat avec/sans perte
* Nouvelle stat: zones épuisées

###v3.8
* Modification texte
* Alerte 'RC détaillé inutile' décalé
* Initiation fonction compatibilité 'RC détaillé dans fenêtre'

###v3.7
* Correction texte
* Correction bugs affichage BBCode
* Correction largeur liste_position
* Modif séparateur milliers (confusion avec décimales)
* Nouvelle stat: expé/jour
* Nouvelle table de statistiques

###v3.6:
* Option afficher/masquer table 'items'
* Remaniement des commentaires et compactage du code
* Correction titre graph
* La 'table_secondaire' ne s'affiche plus au-dessous des options
* Ajout d'une table positions visitées + adaptation/MAJ
* Correction erreur d'affichage date
* Si RC attend d'être ouvert, alerte s'affiche sur son entrée page 'messages'
* Remplacement des 3 fonctions date+idExiste en une seule elementExiste
* Extraction des couleurs des variables 'texte'
* Extraction des coûts de vaisseaux des variables 'texte'
* Gain par booster ajouté

###v3.5.1:
* Corrections mineure

###v3.5:
* Ajout mot-clé
* Prise en charge des items
* Règlage auto délai conservation msg (détection commandant)
* Ajout menu 'trou noir': y entrer détails flotte perdue
* Suppression ligne 'flottes perdues trou noir'
* Version courante dans BBCode
* Ajustements divers

###v3.4.3:
* Ajout mot-clé

###v3.4.2:
* Ajout mot-clé

###v3.4.1:
* Ajout texte mineur

###v3.4:
* Modification fonctionnememnt options
* Numéro de version dans le titre
* BBCode

###v3.3.3:
* Remaniement complet de l'intro du script (detection version...)

###v3.3.2:
* Fonction debug changt de version
* Affichage console pour config_user par default

###v3.3.1:
* Version test, suppr proto soustractionTables()

###v3.3:
* Modif texte
* 1000ème ligne dépassée !
* Ajout mot-clé
* Uniformisation des dates
* Suppression dateFormatOgame2nombre() + Date.prototype.toNombre
* Ajout 'points/j'
* Correction bug bouton radio
* Couleur fond noire 'graph' et 'options'
* La fenêtre 'options' se ferme après changement
* Ajout d'un bouton MAJ
* Inclusion de la page userScript vec vérif MAJ
* Mise en place d'une variable "versionCournte"
* a=a+b -> a+=b

###v3.2:
* Mélange lvl1/lvl2 'alerte position épuisée' (manque infos)
* Alerte 'phrase non reconnue' pour les 'alertes position épuisée'

###v3.1:
* Valeurs actuelles dans les options
* Alerte si position épuisée
* Correction texte mineure
* %points gagnés ressources/flotte

###v3:
* Uniformisation des masquages
* Ajout conservationMessages_marge
* Ajout mot-clé
* Correction bug console<>installation
* Modification du taux de refresh 'config_user.delaiActualisation' de 1500 à 900ms
* Ajout d'une interface utilisateur pour modifier les valeurs par défaut
* Ajout de composés CSS

###v2.4:
* Ajout de 2 boutons liens documentation
* Graphisme des boutons

###v2.3:
* Ajout d'une console d'affichage

###v2.2.2:
* Ajout de purge_message_RC

###v2.2.1:
* Arrangement initialisation
* Externalisation des données textes restantes

###v2.2:
* Modification du bouton 'spoiler' et restriction de son usage
* Ajout d'un bouton interctif 'install'

###v2.1.1:
* modif mot-clé

###v2.1:
* Ajout rubrique 'bugs connus'
* Ajout d'une phrase-clé
* Suppression variable 'etat'
* Suppression bug affichage navigation pages 'message'

###v2:
* Simplification de l'initialisation utilisateur
* Modification du taux de refresh 'config_user.delaiActualisation' de 2000 à 1500ms
* Affichage: Suppression vaisseaux gagnés pour EDLM,VC,Rcy
* Affichage: Alternance couleurs lignes
* Introduction de la variable 'config.conservationMessages'
* Allègement données sauvegardées: division du tableau d'objets 'liste_message' en 3 variables persistantes
* Allègement données sauvegardées: nettoyage systématique des données périmées 
* correction date 1er message (correction rétrocompatible)
* Ajout d'une fonction de MAJ ###v1->###v2

###v1.10.2:
* Correction mineure

###v1.10.1:
* Correction mineure

###v1.10:
* Structuration de l'appel du spoiler
* Ajout du paramètre utilisateur 'spoilerDefault'
* Ajout + correction de mots-clés
* 2 couleurs d'alerte
* Ajout d'un message d'erreur pour les phrases inconnues

###v1.9.1:
* Simplification du code du spoiler

###v1.9:
* Ajout d'un spoiler

###v1.8:
* Ajout d'une ligne 'trou noir' dans la table 'flotte'

###v1.7.2:
* Ajout d'un mot-clé

###v1.7.1:
* Réctification mineure

###v1.7:
* Rétablissement de l'affichage après 'contentWrapper' en raisons de retours de bugs
* Appel direct à la variable temporaire "liste_message" pour le compteur du compteur

###v1.6:
* Ajout d'un compteur du compteur

###v1.5:
* Ajout d'un mot-clé
* Correction d'un bug d'affichage

###v1.4.1:
* Ajout d'un mot-clé

###v1.4:
* Ajout de mots-clés
* Quelques modifications interface/langue
* Ajout d'un graphique des résultats

###v1.3:
* Correction bug: sonde non prise en compte
* Ajout de mots-clés pour la détection des REx
* Prise en charge d'éventuels bugs mots-clés

###v1.2:
* Corrections mineures
* Si consulter un RC détaillé n'apporte rien: alerte

###v1.1:
* Compatibilité Firefox
* Corrections mineures

###v1:
* première version stable